import java.util.Scanner;

public class Menu{
	public static void main (String[] args){
		Grafo g = new Grafo();
		int fop, idop, sop, cl;
		int op[] = new int[4];
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Escolha uma opcao:");
		System.out.println("1 - Criar Transacao");
		System.out.println("2 - Listar Transacoes");
		fop = reader.nextInt();
		
		if(fop == 1){
			//cria uma transacao e a insere no grafo
			System.out.println("Defina uma ID: ");
			idop = reader.nextInt();
			Transacao ntr = new Transacao(idop);
			
			/*if(!g.getAll().isEmpty()){
				for(int i=0;i<g.getAll().size();i++){
					if(g.All.get(i).getId() == idop){
						System.out.println("Ja existe uma transacao com esse ID!");
						System.out.println("Digite outra ID: ");
						idop = reader.nextInt();
						ntr.setId(idop);
					}
				}
			}*/
			
			g.insertTrans(ntr, g.getTR_Iniciada());
			g.insertTrans(ntr, g.getAll());
		}
		else if(fop == 2){
			//lista todas as transacoes do grafo
			Transacao tr = new Transacao(0);
			for(int i=0;i<g.getAll().size();i++){
				tr = g.getAll().get(i);
				System.out.println(tr.getId());
			}
			
			//pede para o usuario escolher uma ID para que as possiveis acoes sejam listadas
			System.out.println("Escolha uma ID: ");
			idop = reader.nextInt();
			
			for(int i=0;i<g.getAll().size();i++){
				tr = g.getAll().get(i);
				if(tr.getId() == idop){
					op = g.checkOption(idop);
				}
			}
			
			for(int c=0;c<4;c++){
				if(op[c] == 1){
					System.out.println("1 - Read");
				}
				else if(op[c] == 2){
					System.out.println("2 - Write");
				}
				else if(op[c] == 3){
					System.out.println("3 - TR_Terminate");
				}
				else if(op[c] == 4){
					System.out.println("4 - TR_Rollback");
				}
				else if(op[c] == 5){
					System.out.println("5 - TR_Commit");
				}
				else if(op[c] == 6){
					System.out.println("6 - TR_Finish");
				}
				else if(op[c] == 7){
					System.out.println("7 - TR_End");
				}
				else if(op[c] == 8){
					System.out.println("8 - TR_Restart");
				}
			}
			//pede uma acao para o usuario
			System.out.println("Escolha uma acao: ");
			sop = reader.nextInt();
			
			//realiza a acao removendo a transacao do no atual e a inserindo no no de acordo com a acao escolhida
			//deleta transacao do estado atual
			cl = g.checkList(idop);
			if(cl == 1){
				g.delTrans(tr, g.getTR_Iniciada());
			}
			else if(cl == 2){
				g.delTrans(tr, g.getAtivo());
			}
			else if(cl == 3){
				g.delTrans(tr, g.getProcesso_Cancelamento());
			}
			else if(cl == 4){
				g.delTrans(tr, g.getProcesso_Efetivacao());
			}
			else if(cl == 5){
				g.delTrans(tr, g.getEfetivada());
			}
			else if(cl == 6){
				g.delTrans(tr, g.getTR_Finalizada());
			}
			
			//incluo a transacao no novo estado
			if(sop == 1){
				tr.read();
				g.insertTrans(tr, g.getAtivo());
			}
			else if(sop == 2){
				tr.write();
				g.insertTrans(tr, g.getAtivo());
			}
			else if(sop == 3){
				//ve se tudo que foi lido foi escrito para que a transacao seja efetivada
				if(tr.count == 0){
					g.insertTrans(tr, g.getProcesso_Efetivacao());
				}
				else{
					g.insertTrans(tr, g.getAtivo());
				}
			}
			else if(sop == 4){
				//ve se tudo que foi lido foi escrito para que a transacao seja efetivada
				if(tr.count == 0){
					g.insertTrans(tr, g.getProcesso_Cancelamento());
				}
				else{
					g.insertTrans(tr, g.getAtivo());
				}
			}
			else if(sop == 5){
				g.insertTrans(tr, g.getEfetivada());
			}
			else if(sop == 6 || sop ==7){
				g.insertTrans(tr, g.getTR_Finalizada());
			}
			else if(sop == 8){
				g.insertTrans(tr, g.getTR_Iniciada());
			}
		}
		
		reader.close();
	}
}
