public class Transacao {
	int id;
	int count;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}

	public Transacao(int id){
		this.id = id;
		this.count = 0;
	}
	
	public void read(){
		this.count++;
	}
	
	public void write(){
		this.count--;
	}
}
