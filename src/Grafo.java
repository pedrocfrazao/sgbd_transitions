import java.util.ArrayList;
import java.util.List;

public class Grafo {
	List<Transacao> TR_Iniciada;
	List<Transacao> Ativo;
	List<Transacao> Processo_Cancelamento;
	List<Transacao> Processo_Efetivacao;
	List<Transacao> Efetivada;
	List<Transacao> TR_Finalizada;
	List<Transacao> All;
	
	public Grafo(){
		this.TR_Iniciada = new ArrayList<Transacao>();
		this.Ativo = new ArrayList<Transacao>();
		this.Processo_Cancelamento = new ArrayList<Transacao>();
		this.Processo_Efetivacao = new ArrayList<Transacao>();
		this.Efetivada = new ArrayList<Transacao>();
		this.TR_Finalizada = new ArrayList<Transacao>();
		this.All = new ArrayList<Transacao>();
	}
	
	public List<Transacao> getTR_Iniciada() {
		return TR_Iniciada;
	}
	public void setTR_Iniciada(List<Transacao> tR_Iniciada) {
		TR_Iniciada = tR_Iniciada;
	}
	public List<Transacao> getAtivo() {
		return Ativo;
	}
	public void setAtivo(List<Transacao> ativo) {
		Ativo = ativo;
	}
	public List<Transacao> getProcesso_Cancelamento() {
		return Processo_Cancelamento;
	}
	public void setProcesso_Cancelamento(List<Transacao> processo_Cancelamento) {
		Processo_Cancelamento = processo_Cancelamento;
	}
	public List<Transacao> getProcesso_Efetivacao() {
		return Processo_Efetivacao;
	}
	public void setProcesso_Efetivacao(List<Transacao> processo_Efetivacao) {
		Processo_Efetivacao = processo_Efetivacao;
	}
	public List<Transacao> getEfetivada() {
		return Efetivada;
	}
	public void setEfetivada(List<Transacao> efetivada) {
		Efetivada = efetivada;
	}
	public List<Transacao> getTR_Finalizada() {
		return TR_Finalizada;
	}
	public void setTR_Finalizada(List<Transacao> tR_Finalizada) {
		TR_Finalizada = tR_Finalizada;
	}
	public List<Transacao> getAll(){
		return All;
	}
	public void setAll(List<Transacao> all){
		All = all;
	}
	
	public void insertTrans(Transacao t, List<Transacao> l){
		l.add(t);
		if(!All.contains(t)){
			All.add(t);
		}
	}
	
	public void delTrans(Transacao t, List<Transacao> l){
		l.remove(t);
	}
	
	public int[] checkOption(int id){
		int i,j;
		int op[] = new int[4];
		for(j=0;j<4;j++){
			op[j] = 0;
		}
		Transacao tr;
		for(i=0;i<TR_Iniciada.size();i++){
			tr = TR_Iniciada.get(i);
			if(tr.getId() == id){
				op[0] = 1;
				op[1] = 2;
				return op;
			}
			else break;
		}
		for(j=0;j<4;j++){
			op[j] = 0;
		}
		for(i=0;i<Ativo.size();i++){
			tr = Ativo.get(i);
			if(tr.getId() == id){
				op[0] = 1;
				op[1] = 2;
				op[2] = 3;
				op[3] = 4;
				return op;
			}
			else break;
		}
		for(j=0;j<4;j++){
			op[j] = 0;
		}
		for(i=0;i<Processo_Cancelamento.size();i++){
			tr = Processo_Cancelamento.get(i);
			if(tr.getId() == id){
				op[0] = 6;
				return op;
			}
			else break;
		}
		for(j=0;j<4;j++){
			op[j] = 0;
		}
		for(i=0;i<Processo_Efetivacao.size();i++){
			tr = Processo_Efetivacao.get(i);
			if(tr.getId() == id){
				op[0] = 4;
				op[1] = 5;
				return op;
			}
			else break;
		}
		for(j=0;j<4;j++){
			op[j] = 0;
		}
		for(i=0;i<Efetivada.size();i++){
			tr = Efetivada.get(i);
			if(tr.getId() == id){
				op[0] = 6;
				return op;
			}
			else break;
		}
		for(j=0;j<4;j++){
			op[j] = 0;
		}
		for(i=0;i<TR_Finalizada.size();i++){
			tr = TR_Finalizada.get(i);
			if(tr.getId() == id){
				op[0] = 7;
				op[1] = 8;
				return op;
			}
			else break;
		}
		for(j=0;j<4;j++){
			op[j] = 0;
		}
		return null;
	}
	
	public int checkList(int id){
		int i,j;
		Transacao tr;
		for(i=0;i<TR_Iniciada.size();i++){
			tr = TR_Iniciada.get(i);
			if(tr.getId() == id){
				return 1;
			}
			else break;
		}
		for(i=0;i<Ativo.size();i++){
			tr = Ativo.get(i);
			if(tr.getId() == id){
				return 2;
			}
			else break;
		}
		for(i=0;i<Processo_Cancelamento.size();i++){
			tr = Processo_Cancelamento.get(i);
			if(tr.getId() == id){
				return 3;
			}
			else break;
		}
		for(i=0;i<Processo_Efetivacao.size();i++){
			tr = Processo_Efetivacao.get(i);
			if(tr.getId() == id){
				return 4;
			}
			else break;
		}
		for(i=0;i<Efetivada.size();i++){
			tr = Efetivada.get(i);
			if(tr.getId() == id){
				return 5;
			}
			else break;
		}
		for(i=0;i<TR_Finalizada.size();i++){
			tr = TR_Finalizada.get(i);
			if(tr.getId() == id){
				return 6;
			}
			else break;
		}
		return 0;
	}
}
